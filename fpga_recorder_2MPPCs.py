import serial
import time
import sys
import os
import subprocess
import httplib
import urllib
import math

print "Welcome to Prof. Leo's program"
time.sleep(1)
#----Serial port Arduino--------------------
serialPath = "/dev/ttyUSB3" #change if program doesnt start
#----Openning Serial port-------------------
print "Connecting..."
fpga = serial.Serial(serialPath, timeout = None, baudrate = 19200, bytesize=serial.EIGHTBITS, stopbits =serial.STOPBITS_ONE, parity=serial.PARITY_NONE)
print "Connection OK"
time.sleep(2)

# preparing file to write
f = open('log.txt', 'w')
date = time.strftime("%d/%b/%Y")
date2 = time.strftime("%H:%M:%S")
f.write('unixt: ' + date + '\n')
f.write('Date: ' + date + '\n')
f.write('Time: ' + date2 + '\n\n')
f.write("Unixtime c0 c1 c2 c3 f0 f1 f2 f3 c03 c013 c023 T\n")
f.close()
time.sleep(1)
print "Text file ready"

sum_det03 = 0
sum_det013 = 0
sum_det023 = 0
sum_freq1 = 0
sum_freq2 = 0
n_events = 0
eff_013 = 0
eff_023 = 0
rate_1 = 0
rate_2 = 0
error_1 = 0
error_2 = 0

# change time according to your preference - leo richards
max_time = 50
start_time = time.time()

n_medition = input("Enter the instance of your medition (ex: 1 for your first medition at a given voltage): ")
voltage1 = input("Enter the input voltage of scintillator 1: ")  
voltage2 = input("Enter the input voltage of scintillator 2: ")  

while ( time.time() - start_time < max_time ):
    
    #----Waiting for Data----------------
    fpga.inWaiting()
    aux = fpga.read()
    aux_h = aux.encode('hex')
    
    if(str(aux_h) == "6e"):

        msg_received = fpga.read(24) 
        det0 = int(str(msg_received[1].encode('hex')) + str(msg_received[0].encode('hex')),16)
        det1 = int(str(msg_received[3].encode('hex')) + str(msg_received[2].encode('hex')),16)
        det2 = int(str(msg_received[5].encode('hex')) + str(msg_received[4].encode('hex')),16)
        det3 = int(str(msg_received[7].encode('hex')) + str(msg_received[6].encode('hex')),16)
        freq0 = int(str(msg_received[9].encode('hex')) + str(msg_received[8].encode('hex')),16)
        freq1 = float(int(str(msg_received[11].encode('hex')) + str(msg_received[10].encode('hex')),16)) #freq1,freq2,det03,det013,det023 como float
        freq2 = float(int(str(msg_received[13].encode('hex')) + str(msg_received[12].encode('hex')),16))
        freq3 = int(str(msg_received[15].encode('hex')) + str(msg_received[14].encode('hex')),16)
        det03 = float(int(str(msg_received[17].encode('hex')) + str(msg_received[16].encode('hex')),16))
        det013 = float(int(str(msg_received[19].encode('hex')) + str(msg_received[18].encode('hex')),16))
        det023 = float(int(str(msg_received[21].encode('hex')) + str(msg_received[20].encode('hex')),16))
        tiempo = float(int(str(msg_received[23].encode('hex')) + str(msg_received[22].encode('hex')),16))/10.0
        print str(det0) + " " + str(det1) + " " + str(det2) + " " + str(det3) + " " + str(freq0) + " " + str(freq1) + " " + str(freq2) + " " + str(freq3) + " " + str(det03) + " " + str(det013) + " " + str(det023) + " " + str(tiempo)
        f = open('log.txt', 'a')
        timestamp = int(time.time())
        f.write(str(timestamp) + " " + str(det0) + " " + str(det1) + " " + str(det2) + " " + str(det3) + " " + str(freq0) + " " + str(freq1) + " " + str(freq2) + " " + str(freq3) + " " + str(det03) + " " + str(det013) + " " + str(det023) + " " + str(tiempo) + "\n")
        f.close()
        
        n_events += 1
        sum_det03 = det03
        sum_det013 = det013
        sum_det023 = det023
        sum_freq1 += freq1
        sum_freq2 += freq2        

if (sum_det03 != 0): 
    eff_013 = sum_det013 / sum_det03 #0<eff<1
    eff_023 = sum_det023 / sum_det03
if (n_events != 0): 
    rate_1 = sum_freq1 / n_events
    rate_2 = sum_freq2 / n_events
if (sum_det03 != 0):
    error_1 = 100*math.sqrt( eff_013*(eff_013+1) / det03 ) #Se calcula el error con la eficiencia 0<eff<1, y luego se ponderan por 100 las effs y los errores.
    error_2 = 100*math.sqrt( eff_023*(eff_023+1) / det03 )
    eff_013 = eff_013*100
    eff_023 = eff_023*100

print "Medition %i" % n_medition
print "Working point scintillator 1 at V = %f" % voltage1
print "Working point scintillator 2 at V = %f" % voltage2
print "efficiency test.1 %f" % eff_013 #%d cambiados por %f
print "efficiency test.2 %f" % eff_023
print "rate test.1 %f" % rate_1
print "rate test.2 %f" % rate_2
print "error test.1 %f" % error_1
print "error test.2 %f" % error_2

#writting files
f_EffRate_test1 = open('dat/EffRate_test1_%i.dat' % n_medition, 'a')
f_EffRate_test1.write(str(rate_1) + " " + str(eff_013) )
f_EffRate_test1.close()
f_EffRate_test2 = open('dat/EffRate_test2_%i.dat' % n_medition, 'a')
f_EffRate_test2.write(str(rate_2) + " " + str(eff_023) )
f_EffRate_test2.close()
f_EffVol_test1 = open('dat/EffVol_test1_%i.dat' % n_medition, 'a')
f_EffVol_test1.write(str(voltage1) + " " + str(eff_013) )
f_EffVol_test1.close()
f_EffVol_test2 = open('dat/EffVol_test2_%i.dat' % n_medition, 'a')
f_EffVol_test2.write(str(voltage2) + " " + str(eff_023) )
f_EffVol_test2.close()
f_EffError_test1 = open('dat/EffError_test1_%i.dat' % n_medition, 'a')
f_EffError_test1.write(str(error_1) + " " + str(eff_013) )
f_EffError_test1.close()
f_EffError_test2 = open('dat/EffError_test2_%i.dat' % n_medition, 'a')
f_EffError_test2.write(str(error_2) + " " + str(eff_023) )
f_EffError_test2.close()

print "Exit after %i seconds" % max_time 
fpga.close()
exit()
