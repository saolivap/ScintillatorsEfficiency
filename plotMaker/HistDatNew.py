import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")
ROOT.SetAtlasStyle()
from ROOT import gPad

basepath = '/home/rygonzalez/ScintillatorsEfficiency/tables/'
files = ['EffRate_MPPC345','EffRate_MPPC969','EffRate_MPPC972','EffRate_MPPC973','EffRate_MPPC990','EffRate_MPPC992','EffRate_MPPC993','EffRate_MPPC1000','EffRate_MPPC1001','EffRate_MPPC1004','EffRate_MPPC1005','EffRate_MPPC1007','EffRate_MPPC1010','EffRate_MPPC1011','EffRate_MPPC1019','EffRate_MPPC1023','EffRate_MPPC1032','EffRate_MPPC1034','EffRate_MPPC1038','EffRate_MPPC1048','EffRate_MPPC1050','EffRate_MPPC1058','EffRate_MPPC1099','EffRate_MPPC1105','EffRate_MPPC1107','EffRate_MPPC1109','EffRate_MPPC1119','EffRate_MPPC1120','EffRate_MPPC1130','EffRate_MPPC1131','EffRate_MPPC1232','EffRate_MPPC1340','EffRate_MPPC1348','EffRate_MPPC1349','EffRate_MPPC1356','EffRate_MPPC1357','EffRate_MPPC1369','EffRate_MPPC1375','EffRate_MPPC1376','EffRate_MPPC1377','EffRate_MPPC1382','EffRate_MPPC1383','EffRate_MPPC1405','EffRate_MPPC1409','EffRate_MPPC1412','EffRate_MPPC1415','EffRate_MPPC1427','EffRate_MPPC1429','EffRate_MPPC1453','EffRate_MPPC1476','EffRate_MPPC1493','EffRate_MPPC1495','EffRate_MPPC1497','EffRate_MPPC1551','EffRate_MPPC1556','EffRate_MPPC1558','EffRate_MPPC1559','EffRate_MPPC1562','EffRate_MPPC?','EffVol_MPPC345','EffVol_MPPC969','EffVol_MPPC972','EffVol_MPPC973','EffVol_MPPC990','EffVol_MPPC992','EffVol_MPPC993','EffVol_MPPC1000','EffVol_MPPC1001','EffVol_MPPC1004','EffVol_MPPC1005','EffVol_MPPC1007','EffVol_MPPC1010','EffVol_MPPC1011','EffVol_MPPC1019','EffVol_MPPC1023','EffVol_MPPC1032','EffVol_MPPC1034','EffVol_MPPC1038','EffVol_MPPC1048','EffVol_MPPC1050','EffVol_MPPC1058','EffVol_MPPC1099','EffVol_MPPC1105','EffVol_MPPC1107','EffVol_MPPC1109','EffVol_MPPC1119','EffVol_MPPC1120','EffVol_MPPC1130','EffVol_MPPC1131','EffVol_MPPC1232','EffVol_MPPC1340','EffVol_MPPC1348','EffVol_MPPC1349','EffVol_MPPC1356','EffVol_MPPC1357','EffVol_MPPC1369','EffVol_MPPC1375','EffVol_MPPC1376','EffVol_MPPC1377','EffVol_MPPC1382','EffVol_MPPC1383','EffVol_MPPC1405','EffVol_MPPC1409','EffVol_MPPC1412','EffVol_MPPC1415','EffVol_MPPC1427','EffVol_MPPC1429','EffVol_MPPC1453','EffVol_MPPC1476','EffVol_MPPC1493','EffVol_MPPC1495','EffVol_MPPC1497','EffVol_MPPC1551','EffVol_MPPC1556','EffVol_MPPC1558','EffVol_MPPC1559','EffVol_MPPC1562','EffVol_MPPC?']
plots = ['mg_EffRate','mg_EffVol','mg_EffRate_Good','mg_EffRate_Bad']

p = {}
for iplot in plots:
    p[iplot] = {}
    p[iplot] = ROOT.TMultiGraph()

    g = {}
    for ifile in files:
        g[ifile] = {}
        name = '%s%s' % (basepath, ifile)
        g[ifile] = ROOT.TGraph( name + '.dat' )
        g[ifile].GetYaxis().SetTitleOffset(1.2)
        g[ifile].GetXaxis().SetTitleOffset(1.2)
        # line and marker style
        g[ifile].SetMarkerSize(2)
        g[ifile].SetLineWidth(1)
        g[ifile].SetLineStyle(7)
        g[ifile].SetMarkerStyle(ROOT.kFullCircle)
        # line and marker colour
        if ifile == 'EffRate_MPPC345':
            g[ifile].SetMarkerColor(ROOT.kOrange)
            g[ifile].SetLineColor(ROOT.kOrange)
            EffRate_MPPC345 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC345':
            g[ifile].SetMarkerColor(ROOT.kOrange)
            g[ifile].SetLineColor(ROOT.kOrange)
            EffVol_MPPC345 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC969':
            g[ifile].SetMarkerColor(ROOT.kOrange-3)
            g[ifile].SetLineColor(ROOT.kOrange-3)
            EffRate_MPPC969 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC969':
            g[ifile].SetMarkerColor(ROOT.kOrange-3)
            g[ifile].SetLineColor(ROOT.kOrange-3)
            EffVol_MPPC969 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC972':
            g[ifile].SetMarkerColor(ROOT.kOrange+4)
            g[ifile].SetLineColor(ROOT.kOrange+4)
            EffRate_MPPC972 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC972':
            g[ifile].SetMarkerColor(ROOT.kOrange+4)
            g[ifile].SetLineColor(ROOT.kOrange+4)
            EffVol_MPPC972 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC973':
            g[ifile].SetMarkerColor(ROOT.kOrange+10)
            g[ifile].SetLineColor(ROOT.kOrange+10)
            EffRate_MPPC973 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC973':
            g[ifile].SetMarkerColor(ROOT.kOrange+10)
            g[ifile].SetLineColor(ROOT.kOrange+10)
            EffVol_MPPC973 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC990':
            g[ifile].SetMarkerColor(ROOT.kOrange-1)
            g[ifile].SetLineColor(ROOT.kOrange-1)
            EffRate_MPPC990 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC990':
            g[ifile].SetMarkerColor(ROOT.kOrange-1)
            g[ifile].SetLineColor(ROOT.kOrange-1)
            EffVol_MPPC990 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC992':
            g[ifile].SetMarkerColor(ROOT.kOrange-6)
            g[ifile].SetLineColor(ROOT.kOrange-6)
            EffRate_MPPC992 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC992':
            g[ifile].SetMarkerColor(ROOT.kOrange-6)
            g[ifile].SetLineColor(ROOT.kOrange-6)
            EffVol_MPPC992 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC993':
            g[ifile].SetMarkerColor(ROOT.kOrange+2)
            g[ifile].SetLineColor(ROOT.kOrange+2)
            EffRate_MPPC993 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC993':
            g[ifile].SetMarkerColor(ROOT.kOrange+2)
            g[ifile].SetLineColor(ROOT.kOrange+2)
            EffVol_MPPC993 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1000':
            g[ifile].SetMarkerColor(ROOT.kCyan)
            g[ifile].SetLineColor(ROOT.kCyan)
            EffRate_MPPC1000 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1000':
            g[ifile].SetMarkerColor(ROOT.kCyan)
            g[ifile].SetLineColor(ROOT.kCyan)
            EffVol_MPPC1000 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1001':
            g[ifile].SetMarkerColor(ROOT.kCyan+2)
            g[ifile].SetLineColor(ROOT.kCyan+2)
            EffRate_MPPC1001 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1001':
            g[ifile].SetMarkerColor(ROOT.kCyan+2)
            g[ifile].SetLineColor(ROOT.kCyan+2)
            EffVol_MPPC1001 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1004':
            g[ifile].SetMarkerColor(ROOT.kCyan+3)
            g[ifile].SetLineColor(ROOT.kCyan+3)
            EffRate_MPPC1004 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1004':
            g[ifile].SetMarkerColor(ROOT.kCyan+3)
            g[ifile].SetLineColor(ROOT.kCyan+3)
            EffVol_MPPC1004 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1005':
            g[ifile].SetMarkerColor(ROOT.kCyan+4)
            g[ifile].SetLineColor(ROOT.kCyan+4)
            EffRate_MPPC1005 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1005':
            g[ifile].SetMarkerColor(ROOT.kCyan+4)
            g[ifile].SetLineColor(ROOT.kCyan+4)
            EffVol_MPPC1005 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1007':
            g[ifile].SetMarkerColor(ROOT.kCyan-5)
            g[ifile].SetLineColor(ROOT.kCyan-5)
            EffRate_MPPC1007 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1007':
            g[ifile].SetMarkerColor(ROOT.kCyan-5)
            g[ifile].SetLineColor(ROOT.kCyan-5)
            EffVol_MPPC1007 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1010':
            g[ifile].SetMarkerColor(ROOT.kBlue)
            g[ifile].SetLineColor(ROOT.kBlue)
            EffRate_MPPC1010 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1010':
            g[ifile].SetMarkerColor(ROOT.kBlue)
            g[ifile].SetLineColor(ROOT.kBlue)
            EffVol_MPPC1010 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1011':
            g[ifile].SetMarkerColor(ROOT.kBlue-4)
            g[ifile].SetLineColor(ROOT.kBlue-4)
            EffRate_MPPC1011 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1011':
            g[ifile].SetMarkerColor(ROOT.kBlue-4)
            g[ifile].SetLineColor(ROOT.kBlue-4)
            EffVol_MPPC1011 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1019':
            g[ifile].SetMarkerColor(ROOT.kBlue+3)
            g[ifile].SetLineColor(ROOT.kBlue+3)
            EffRate_MPPC1019 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1019':
            g[ifile].SetMarkerColor(ROOT.kBlue+3)
            g[ifile].SetLineColor(ROOT.kBlue+3)
            EffVol_MPPC1019 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1023':
            g[ifile].SetMarkerColor(ROOT.kBlue+4)
            g[ifile].SetLineColor(ROOT.kBlue+4)
            EffRate_MPPC1023 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1023':
            g[ifile].SetMarkerColor(ROOT.kBlue+4)
            g[ifile].SetLineColor(ROOT.kBlue+4)
            EffVol_MPPC1023 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1032':
            g[ifile].SetMarkerColor(ROOT.kBlue-7)
            g[ifile].SetLineColor(ROOT.kBlue-7)
            EffRate_MPPC1032 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1032':
            g[ifile].SetMarkerColor(ROOT.kBlue-7)
            g[ifile].SetLineColor(ROOT.kBlue-7)
            EffVol_MPPC1032 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1034':
            g[ifile].SetMarkerColor(ROOT.kBlue-1)
            g[ifile].SetLineColor(ROOT.kBlue-1)
            EffRate_MPPC1034 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1034':
            g[ifile].SetMarkerColor(ROOT.kBlue-1)
            g[ifile].SetLineColor(ROOT.kBlue-1)
            EffVol_MPPC1034 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1038':
            g[ifile].SetMarkerColor(ROOT.kBlue-5)
            g[ifile].SetLineColor(ROOT.kBlue-5)
            EffRate_MPPC1038 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1038':
            g[ifile].SetMarkerColor(ROOT.kBlue-5)
            g[ifile].SetLineColor(ROOT.kBlue-5)
            EffVol_MPPC1038 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1048':
            g[ifile].SetMarkerColor(ROOT.kViolet)
            g[ifile].SetLineColor(ROOT.kViolet)
            EffRate_MPPC1048 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1048':
            g[ifile].SetMarkerColor(ROOT.kViolet)
            g[ifile].SetLineColor(ROOT.kViolet)
            EffVol_MPPC1048 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1050':
            g[ifile].SetMarkerColor(ROOT.kViolet-1)
            g[ifile].SetLineColor(ROOT.kViolet-1)
            EffRate_MPPC1050 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1050':
            g[ifile].SetMarkerColor(ROOT.kViolet-1)
            g[ifile].SetLineColor(ROOT.kViolet-1)
            EffVol_MPPC1050 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1058':
            g[ifile].SetMarkerColor(ROOT.kViolet-4)
            g[ifile].SetLineColor(ROOT.kViolet-4)
            EffRate_MPPC1058 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1058':
            g[ifile].SetMarkerColor(ROOT.kViolet-4)
            g[ifile].SetLineColor(ROOT.kViolet-4)
            EffVol_MPPC1058 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1099':
            g[ifile].SetMarkerColor(ROOT.kViolet-6)
            g[ifile].SetLineColor(ROOT.kViolet-6)
            EffRate_MPPC1099 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1099':
            g[ifile].SetMarkerColor(ROOT.kViolet-6)
            g[ifile].SetLineColor(ROOT.kViolet-6)
            EffVol_MPPC1099 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1105':
            g[ifile].SetMarkerColor(ROOT.kViolet-7)
            g[ifile].SetLineColor(ROOT.kViolet-7)
            EffRate_MPPC1105 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1105':
            g[ifile].SetMarkerColor(ROOT.kViolet-7)
            g[ifile].SetLineColor(ROOT.kViolet-7)
            EffVol_MPPC1105 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1107':
            g[ifile].SetMarkerColor(ROOT.kViolet-9)
            g[ifile].SetLineColor(ROOT.kViolet-9)
            EffRate_MPPC1107 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1107':
            g[ifile].SetMarkerColor(ROOT.kViolet-9)
            g[ifile].SetLineColor(ROOT.kViolet-9)
            EffVol_MPPC1107 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1109':
            g[ifile].SetMarkerColor(ROOT.kViolet+9)
            g[ifile].SetLineColor(ROOT.kViolet+9)
            EffRate_MPPC1109 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1109':
            g[ifile].SetMarkerColor(ROOT.kViolet+9)
            g[ifile].SetLineColor(ROOT.kViolet+9)
            EffVol_MPPC1109 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1119':
            g[ifile].SetMarkerColor(ROOT.kViolet+2)
            g[ifile].SetLineColor(ROOT.kViolet+2)
            EffRate_MPPC1119 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1119':
            g[ifile].SetMarkerColor(ROOT.kViolet+2)
            g[ifile].SetLineColor(ROOT.kViolet+2)
            EffVol_MPPC1119 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1120':
            g[ifile].SetMarkerColor(ROOT.kViolet+3)
            g[ifile].SetLineColor(ROOT.kViolet+3)
            EffRate_MPPC1120 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1120':
            g[ifile].SetMarkerColor(ROOT.kViolet+3)
            g[ifile].SetLineColor(ROOT.kViolet+3)
            EffVol_MPPC1120 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1130':
            g[ifile].SetMarkerColor(ROOT.kViolet+7)
            g[ifile].SetLineColor(ROOT.kViolet+7)
            EffRate_MPPC1130 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1130':
            g[ifile].SetMarkerColor(ROOT.kViolet+7)
            g[ifile].SetLineColor(ROOT.kViolet+7)
            EffVol_MPPC1130 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1131':
            g[ifile].SetMarkerColor(ROOT.kViolet-3)
            g[ifile].SetLineColor(ROOT.kViolet-3)
            EffRate_MPPC1131 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1131':
            g[ifile].SetMarkerColor(ROOT.kViolet-3)
            g[ifile].SetLineColor(ROOT.kViolet-3)
            EffVol_MPPC1131 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1232':
            g[ifile].SetMarkerColor(ROOT.kViolet+4)
            g[ifile].SetLineColor(ROOT.kViolet+4)
            EffRate_MPPC1232 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1232':
            g[ifile].SetMarkerColor(ROOT.kViolet+4)
            g[ifile].SetLineColor(ROOT.kViolet+4)
            EffVol_MPPC1232 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1340':
            g[ifile].SetMarkerColor(ROOT.kSpring-2)
            g[ifile].SetLineColor(ROOT.kSpring-2)
            EffRate_MPPC1340 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1340':
            g[ifile].SetMarkerColor(ROOT.kSpring-2)
            g[ifile].SetLineColor(ROOT.kSpring-2)
            EffVol_MPPC1340 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1348':
            g[ifile].SetMarkerColor(ROOT.kGreen)
            g[ifile].SetLineColor(ROOT.kGreen)
            EffRate_MPPC1348 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1348':
            g[ifile].SetMarkerColor(ROOT.kGreen)
            g[ifile].SetLineColor(ROOT.kGreen)
            EffVol_MPPC1348 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1349':
            g[ifile].SetMarkerColor(ROOT.kSpring-9)
            g[ifile].SetLineColor(ROOT.kSpring-9)
            EffRate_MPPC1349 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1349':
            g[ifile].SetMarkerColor(ROOT.kSpring-9)
            g[ifile].SetLineColor(ROOT.kSpring-9)
            EffVol_MPPC1349 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1356':
            g[ifile].SetMarkerColor(ROOT.kGreen-2)
            g[ifile].SetLineColor(ROOT.kGreen-2)
            EffRate_MPPC1356 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1356':
            g[ifile].SetMarkerColor(ROOT.kGreen-2)
            g[ifile].SetLineColor(ROOT.kGreen-2)
            EffVol_MPPC1356 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1357':
            g[ifile].SetMarkerColor(ROOT.kGreen+2)
            g[ifile].SetLineColor(ROOT.kGreen+2)
            EffRate_MPPC1357 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1357':
            g[ifile].SetMarkerColor(ROOT.kGreen+2)
            g[ifile].SetLineColor(ROOT.kGreen+2)
            EffVol_MPPC1357 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1369':
            g[ifile].SetMarkerColor(ROOT.kGreen+4)
            g[ifile].SetLineColor(ROOT.kGreen+4)
            EffRate_MPPC1369 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1369':
            g[ifile].SetMarkerColor(ROOT.kGreen+4)
            g[ifile].SetLineColor(ROOT.kGreen+4)
            EffVol_MPPC1369 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1375':
            g[ifile].SetMarkerColor(ROOT.kYellow)
            g[ifile].SetLineColor(ROOT.kYellow)
            EffRate_MPPC1375 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1375':
            g[ifile].SetMarkerColor(ROOT.kYellow)
            g[ifile].SetLineColor(ROOT.kYellow)
            EffVol_MPPC1375 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1376':
            g[ifile].SetMarkerColor(ROOT.kGreen-3)
            g[ifile].SetLineColor(ROOT.kGreen-3)
            EffRate_MPPC1376 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1376':
            g[ifile].SetMarkerColor(ROOT.kGreen-3)
            g[ifile].SetLineColor(ROOT.kGreen-3)
            EffVol_MPPC1376 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1377':
            g[ifile].SetMarkerColor(ROOT.kYellow-7)
            g[ifile].SetLineColor(ROOT.kYellow-7)
            EffRate_MPPC1377 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1377':
            g[ifile].SetMarkerColor(ROOT.kYellow-7)
            g[ifile].SetLineColor(ROOT.kYellow-7)
            EffVol_MPPC1377 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1382':
            g[ifile].SetMarkerColor(ROOT.kGreen-7)
            g[ifile].SetLineColor(ROOT.kGreen-7)
            EffRate_MPPC1382 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1382':
            g[ifile].SetMarkerColor(ROOT.kGreen-7)
            g[ifile].SetLineColor(ROOT.kGreen-7)
            EffVol_MPPC1382 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1383':
            g[ifile].SetMarkerColor(ROOT.kSpring+10)
            g[ifile].SetLineColor(ROOT.kSpring+10)
            EffRate_MPPC1383 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1383':
            g[ifile].SetMarkerColor(ROOT.kSpring+10)
            g[ifile].SetLineColor(ROOT.kSpring+10)
            EffVol_MPPC1383 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1405':
            g[ifile].SetMarkerColor(ROOT.kPink)
            g[ifile].SetLineColor(ROOT.kPink)
            EffRate_MPPC1405 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1405':
            g[ifile].SetMarkerColor(ROOT.kPink)
            g[ifile].SetLineColor(ROOT.kPink)
            EffVol_MPPC1405 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1409':
            g[ifile].SetMarkerColor(ROOT.kPink-1)
            g[ifile].SetLineColor(ROOT.kPink-1)
            EffRate_MPPC1409 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1409':
            g[ifile].SetMarkerColor(ROOT.kPink-1)
            g[ifile].SetLineColor(ROOT.kPink-1)
            EffVol_MPPC1409 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1412':
            g[ifile].SetMarkerColor(ROOT.kPink-2)
            g[ifile].SetLineColor(ROOT.kPink-2)
            EffRate_MPPC1412 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1412':
            g[ifile].SetMarkerColor(ROOT.kPink-2)
            g[ifile].SetLineColor(ROOT.kPink-2)
            EffVol_MPPC1412 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1415':
            g[ifile].SetMarkerColor(ROOT.kPink+8)
            g[ifile].SetLineColor(ROOT.kPink+8)
            EffRate_MPPC1415 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1415':
            g[ifile].SetMarkerColor(ROOT.kPink+8)
            g[ifile].SetLineColor(ROOT.kPink+8)
            EffVol_MPPC1415 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1427':
            g[ifile].SetMarkerColor(ROOT.kPink-3)
            g[ifile].SetLineColor(ROOT.kPink-3)
            EffRate_MPPC1427 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1427':
            g[ifile].SetMarkerColor(ROOT.kPink-3)
            g[ifile].SetLineColor(ROOT.kPink-3)
            EffVol_MPPC1427 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1429':
            g[ifile].SetMarkerColor(ROOT.kPink-4)
            g[ifile].SetLineColor(ROOT.kPink-4)
            EffRate_MPPC1429 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1429':
            g[ifile].SetMarkerColor(ROOT.kPink-4)
            g[ifile].SetLineColor(ROOT.kPink-4)
            EffVol_MPPC1429 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1453':
            g[ifile].SetMarkerColor(ROOT.kPink-5)
            g[ifile].SetLineColor(ROOT.kPink-5)
            EffRate_MPPC1453 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1453':
            g[ifile].SetMarkerColor(ROOT.kPink-5)
            g[ifile].SetLineColor(ROOT.kPink-5)
            EffVol_MPPC1453 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1476':
            g[ifile].SetMarkerColor(ROOT.kPink+1)
            g[ifile].SetLineColor(ROOT.kPink+1)
            EffRate_MPPC1476 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1476':
            g[ifile].SetMarkerColor(ROOT.kPink+1)
            g[ifile].SetLineColor(ROOT.kPink+1)
            EffVol_MPPC1476 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1493':
            g[ifile].SetMarkerColor(ROOT.kMagenta-2)
            g[ifile].SetLineColor(ROOT.kMagenta-2)
            EffRate_MPPC1493 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1493':
            g[ifile].SetMarkerColor(ROOT.kMagenta-2)
            g[ifile].SetLineColor(ROOT.kMagenta-2)
            EffVol_MPPC1493 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1495':
            g[ifile].SetMarkerColor(ROOT.kPink-6)
            g[ifile].SetLineColor(ROOT.kPink-6)
            EffRate_MPPC1495 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1495':
            g[ifile].SetMarkerColor(ROOT.kPink-6)
            g[ifile].SetLineColor(ROOT.kPink-6)
            EffVol_MPPC1495 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1497':
            g[ifile].SetMarkerColor(ROOT.kMagenta-7)
            g[ifile].SetLineColor(ROOT.kMagenta-7)
            EffRate_MPPC1497 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1497':
            g[ifile].SetMarkerColor(ROOT.kMagenta-7)
            g[ifile].SetLineColor(ROOT.kMagenta-7)
            EffVol_MPPC1497 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1551':
            g[ifile].SetMarkerColor(ROOT.kRed-4)
            g[ifile].SetLineColor(ROOT.kRed-4)
            EffRate_MPPC1551 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1551':
            g[ifile].SetMarkerColor(ROOT.kRed-4)
            g[ifile].SetLineColor(ROOT.kRed-4)
            EffVol_MPPC1551 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1556':
            g[ifile].SetMarkerColor(ROOT.kRed-6)
            g[ifile].SetLineColor(ROOT.kRed-6)
            EffRate_MPPC1556 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1556':
            g[ifile].SetMarkerColor(ROOT.kRed-6)
            g[ifile].SetLineColor(ROOT.kRed-6)
            EffVol_MPPC1556 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1558':
            g[ifile].SetMarkerColor(ROOT.kRed-7)
            g[ifile].SetLineColor(ROOT.kRed-7)
            EffRate_MPPC1558 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1558':
            g[ifile].SetMarkerColor(ROOT.kRed-7)
            g[ifile].SetLineColor(ROOT.kRed-7)
            EffVol_MPPC1558 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1559':
            g[ifile].SetMarkerColor(ROOT.kTeal+1)
            g[ifile].SetLineColor(ROOT.kTeal+1)
            EffRate_MPPC1559 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1559':
            g[ifile].SetMarkerColor(ROOT.kTeal+1)
            g[ifile].SetLineColor(ROOT.kTeal+1)
            EffVol_MPPC1559 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC1562':
            g[ifile].SetMarkerColor(ROOT.kRed+1)
            g[ifile].SetLineColor(ROOT.kRed+1)
            EffRate_MPPC1562 = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC1562':
            g[ifile].SetMarkerColor(ROOT.kRed+1)
            g[ifile].SetLineColor(ROOT.kRed+1)
            EffVol_MPPC1562 = g[ifile].Clone(ifile)
        if ifile == 'EffRate_MPPC?':
            g[ifile].SetMarkerColor(ROOT.kRed-2)
            g[ifile].SetLineColor(ROOT.kRed-2)
            EffRate_MPPC_ = g[ifile].Clone(ifile)
        if ifile == 'EffVol_MPPC?':
            g[ifile].SetMarkerColor(ROOT.kRed-2)
            g[ifile].SetLineColor(ROOT.kRed-2)
            EffVol_MPPC_ = g[ifile].Clone(ifile)
            
        #graphs in each plot
        if iplot == 'mg_EffRate':
            if ifile == 'EffRate_MPPC345' or ifile == 'EffRate_MPPC969' or ifile == 'EffRate_MPPC972' or ifile == 'EffRate_MPPC973' or ifile == 'EffRate_MPPC990' or ifile == 'EffRate_MPPC992' or ifile == 'EffRate_MPPC993' or ifile == 'EffRate_MPPC1000' or ifile == 'EffRate_MPPC1001' or ifile == 'EffRate_MPPC1004' or ifile == 'EffRate_MPPC1005' or ifile == 'EffRate_MPPC1007' or ifile == 'EffRate_MPPC1010' or ifile == 'EffRate_MPPC1011' or ifile == 'EffRate_MPPC1019' or ifile == 'EffRate_MPPC1023' or ifile == 'EffRate_MPPC1032' or ifile == 'EffRate_MPPC1034' or ifile == 'EffRate_MPPC1038' or ifile == 'EffRate_MPPC1048' or ifile == 'EffRate_MPPC1050' or ifile == 'EffRate_MPPC1058' or ifile == 'EffRate_MPPC1099' or ifile == 'EffRate_MPPC1105' or ifile == 'EffRate_MPPC1107' or ifile == 'EffRate_MPPC1109' or ifile == 'EffRate_MPPC1119' or ifile == 'EffRate_MPPC1120' or ifile == 'EffRate_MPPC1130' or ifile == 'EffRate_MPPC1131' or ifile == 'EffRate_MPPC1232' or ifile == 'EffRate_MPPC1340' or ifile == 'EffRate_MPPC1348' or ifile == 'EffRate_MPPC1349' or ifile == 'EffRate_MPPC1356' or ifile == 'EffRate_MPPC1357' or ifile == 'EffRate_MPPC1369' or ifile == 'EffRate_MPPC1375' or ifile == 'EffRate_MPPC1376' or ifile == 'EffRate_MPPC1377' or ifile == 'EffRate_MPPC1382' or ifile == 'EffRate_MPPC1383' or ifile == 'EffRate_MPPC1405' or ifile == 'EffRate_MPPC1409' or ifile == 'EffRate_MPPC1412' or ifile == 'EffRate_MPPC1415' or ifile == 'EffRate_MPPC1427' or ifile == 'EffRate_MPPC1429' or ifile == 'EffRate_MPPC1453' or ifile == 'EffRate_MPPC1476' or ifile == 'EffRate_MPPC1493' or ifile == 'EffRate_MPPC1495' or ifile == 'EffRate_MPPC1497' or ifile == 'EffRate_MPPC1551' or ifile == 'EffRate_MPPC1556' or ifile == 'EffRate_MPPC1558' or ifile == 'EffRate_MPPC1559' or ifile == 'EffRate_MPPC1562' or ifile == 'EffRate_MPPC?':
                p[iplot].Add(g[ifile])

        if iplot == 'mg_EffVol':
            if ifile == 'EffVol_MPPC345' or ifile == 'EffVol_MPPC969' or ifile == 'EffVol_MPPC972' or ifile == 'EffVol_MPPC973' or ifile == 'EffVol_MPPC990' or ifile == 'EffVol_MPPC992' or ifile == 'EffVol_MPPC993' or ifile == 'EffVol_MPPC1000' or ifile == 'EffVol_MPPC1001' or ifile == 'EffVol_MPPC1004' or ifile == 'EffVol_MPPC1005' or ifile == 'EffVol_MPPC1007' or ifile == 'EffVol_MPPC1010' or ifile == 'EffVol_MPPC1011' or ifile == 'EffVol_MPPC1019' or ifile == 'EffVol_MPPC1023' or ifile == 'EffVol_MPPC1032' or ifile == 'EffVol_MPPC1034' or ifile == 'EffVol_MPPC1038' or ifile == 'EffVol_MPPC1048' or ifile == 'EffVol_MPPC1050' or ifile == 'EffVol_MPPC1058' or ifile == 'EffVol_MPPC1099' or ifile == 'EffVol_MPPC1105' or ifile == 'EffVol_MPPC1107' or ifile == 'EffVol_MPPC1109' or ifile == 'EffVol_MPPC1119' or ifile == 'EffVol_MPPC1120' or ifile == 'EffVol_MPPC1130' or ifile == 'EffVol_MPPC1131' or ifile == 'EffVol_MPPC1232' or ifile == 'EffVol_MPPC1340' or ifile == 'EffVol_MPPC1348' or ifile == 'EffVol_MPPC1349' or ifile == 'EffVol_MPPC1356' or ifile == 'EffVol_MPPC1357' or ifile == 'EffVol_MPPC1369' or ifile == 'EffVol_MPPC1375' or ifile == 'EffVol_MPPC1376' or ifile == 'EffVol_MPPC1377' or ifile == 'EffVol_MPPC1382' or ifile == 'EffVol_MPPC1383' or ifile == 'EffVol_MPPC1405' or ifile == 'EffVol_MPPC1409' or ifile == 'EffVol_MPPC1412' or ifile == 'EffVol_MPPC1415' or ifile == 'EffVol_MPPC1427' or ifile == 'EffVol_MPPC1429' or ifile == 'EffVol_MPPC1453' or ifile == 'EffVol_MPPC1476' or ifile == 'EffVol_MPPC1493' or ifile == 'EffVol_MPPC1495' or ifile == 'EffVol_MPPC1497' or ifile == 'EffVol_MPPC1551' or ifile == 'EffVol_MPPC1556' or ifile == 'EffVol_MPPC1558' or ifile == 'EffVol_MPPC1559' or ifile == 'EffVol_MPPC1562' or ifile == 'EffVol_MPPC?':
                p[iplot].Add(g[ifile])

        if iplot == 'mg_EffRate_Good':
            if ifile == 'EffRate_MPPC969' or ifile == 'EffRate_MPPC972' or ifile == 'EffRate_MPPC973' or ifile == 'EffRate_MPPC990' or ifile == 'EffRate_MPPC992' or ifile == 'EffRate_MPPC993' or ifile == 'EffRate_MPPC1000' or ifile == 'EffRate_MPPC1001' or ifile == 'EffRate_MPPC1004' or ifile == 'EffRate_MPPC1005' or ifile == 'EffRate_MPPC1007' or ifile == 'EffRate_MPPC1010' or ifile == 'EffRate_MPPC1011' or ifile == 'EffRate_MPPC1019' or ifile == 'EffRate_MPPC1023' or ifile == 'EffRate_MPPC1032' or ifile == 'EffRate_MPPC1105' or ifile == 'EffRate_MPPC1383' or ifile == 'EffRate_MPPC1357' or ifile == 'EffRate_MPPC1107' or ifile == 'EffRate_MPPC1109' or ifile == 'EffRate_MPPC1119' or ifile == 'EffRate_MPPC1038' or ifile == 'EffRate_MPPC1382' or ifile == 'EffRate_MPPC1048' or ifile == 'EffRate_MPPC1376' or ifile == 'EffRate_MPPC1377' or ifile == 'EffRate_MPPC973' or ifile == 'EffRate_MPPC1405' or ifile == 'EffRate_MPPC972' or ifile == 'EffRate_MPPC1348' or ifile == 'EffRate_MPPC1010' or ifile == 'EffRate_MPPC1034' or ifile == 'EffRate_MPPC1429' or ifile == 'EffRate_MPPC1005' or ifile == 'EffRate_MPPC?' or ifile == 'EffRate_MPPC1562' or ifile == 'EffRate_MPPC992' or ifile == 'EffRate_MPPC1130' or ifile == 'EffRate_MPPC1551' or ifile == 'EffRate_MPPC1556' or ifile == 'EffRate_MPPC1558' or ifile == 'EffRate_MPPC1453' or ifile == 'EffRate_MPPC1497' or ifile == 'EffRate_MPPC1032' or ifile == 'EffRate_MPPC1007' or ifile == 'EffRate_MPPC1099' or ifile == 'EffRate_MPPC1349' or ifile == 'EffRate_MPPC1415' or ifile == 'EffRate_MPPC1476' or ifile == 'EffRate_MPPC1493' or ifile == 'EffRate_MPPC1559':
                p[iplot].Add(g[ifile])

        if iplot == 'mg_EffRate_Bad':
            if ifile == 'EffRate_MPPC1409' or ifile == 'EffRate_MPPC1050' or ifile == 'EffRate_MPPC1427' or ifile == 'EffRate_MPPC1058' or ifile == 'EffRate_MPPC1495' or ifile == 'EffRate_MPPC1412' or ifile == 'EffRate_MPPC1232' or ifile == 'EffRate_MPPC1120' or ifile == 'EffRate_MPPC1131' or ifile == 'EffRate_MPPC1340' or ifile == 'EffRate_MPPC1356' or ifile == 'EffRate_MPPC1369' or ifile == 'EffRate_MPPC1375':
                p[iplot].Add(g[ifile])

    #text in canvas
    texATLAS = ROOT.TLatex(0.42, 0.2, "#bf{#it{ATLAS}} Work in progress") #was 0.63, 0.12
    texATLAS.SetName('latex1')
    texATLAS.SetNDC(True)
    texATLAS.SetTextSize(0.05)
        
    # canvas
    p[iplot].SetMinimum(1)
    p[iplot].SetMaximum(100)
    ct = ROOT.TCanvas('ct', 'ct', 800, 800)
    p[iplot].Draw('ALP')

    if iplot == 'mg_EffRate' or iplot == 'mg_EffRate_Bad':
        ct.SetLogx()
    if iplot == 'mg_EffRate' or iplot == 'mg_EffRate_Good' or iplot == 'mg_EffRate_Bad':
        p[iplot].GetXaxis().SetTitle('Rate (Hz)')
        p[iplot].GetYaxis().SetTitle('Efficiency (%)')
    if iplot == 'mg_EffVol':
        p[iplot].GetXaxis().SetTitle('Voltage (V)')
        p[iplot].GetYaxis().SetTitle('Efficiency (%)')
        p[iplot].GetXaxis().SetLimits(64,67.5)
    if iplot == 'mg_EffRate_Good':
        p[iplot].GetXaxis().SetLimits(1,100)

    texATLAS.Draw()
    ct.Update()
    ct.SaveAs('/home/rygonzalez/ScintillatorsEfficiency/plots/%s.pdf' % (iplot))
