import ROOT
ROOT.gROOT.SetBatch(True)
ROOT.gROOT.LoadMacro("AtlasStyle.C")
ROOT.gROOT.LoadMacro("AtlasLabels.C")
ROOT.gROOT.LoadMacro("AtlasUtils.C")
ROOT.SetAtlasStyle()
from ROOT import gPad
import numpy as np

basepath = '../tables/'
efficiencies = ['Eff_MPPC1001']
rates = ['Rate_MPPC1001']
vols = ['Vol_MPPC1001']
errors = ['Err_MPPC1001']
plots = ['mg_test']

for ierror in errors:
    name_error = '%s%s' % (basepath, ierror)
    v_errorY = np.genfromtxt(name_error + '.dat', delimiter='\n')
for ivol in vols:
    name_vol = '%s%s' % (basepath, ivol)
    v_vol = np.genfromtxt(name_vol + '.dat', delimiter='\n')
for irate in rates:
    name_rate = '%s%s' % (basepath, irate)
    v_rate = np.genfromtxt(name_rate + '.dat', delimiter='\n')
for ieff in efficiencies:
    name_eff = '%s%s' % (basepath, ieff)
    v_eff = np.genfromtxt(name_eff + '.dat', delimiter='\n')
v_errorX = np.empty(len(v_eff), np.dtype('float64'))
for j in range(0, len(v_errorX)):
    v_errorX[j] = 0.5
    
print v_eff
print v_rate
print v_vol
print v_errorY
print v_errorX

gt = ROOT.TGraphErrors(len(v_eff), v_vol, v_eff, v_errorX, v_errorY)
gt.Draw('ALP')
