#!/bin/bash

echo Enter the number of the top tested MPPC:
read scint_num_t1
echo Enter the number of the bottom tested MPPC:
read scint_num_t2

awk '{print $1,$2}' EffRate_test1_{1..10}.dat | column -t >../tables/EffRate_MPPC$scint_num_t1.dat
awk '{print $1,$2}' EffRate_test2_{1..10}.dat | column -t >../tables/EffRate_MPPC$scint_num_t2.dat
awk '{print $1,$2}' EffVol_test1_{1..10}.dat | column -t >../tables/EffVol_MPPC$scint_num_t1.dat
awk '{print $1,$2}' EffVol_test2_{1..10}.dat | column -t >../tables/EffVol_MPPC$scint_num_t2.dat

rm -rf *.dat


